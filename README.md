This is the first version of the 3D VR Google Cardboard controller for the Mia Robocat which is the Dawn Robotics kit as found here http://www.dawnrobotics.co.uk/raspberry-pi-camera-robot-chassis-bundle/ and the software included here https://bitbucket.org/DawnRobotics. It will require a mobile device that supports canvas, web3gl and device orientation.

To avoid a cross-origin error when you will need to add a line to mjpeg_httpd.h within the raspberry_pi_camera_streamer/src and recompile. The line is "Access-Control-Allow-Origin: *\r\n" \. 


```
#!C

#define STD_HEADER "Connection: close\r\n" \
    "Server: Raspberry Pi Camera Streamer/0.01\r\n" \
    "Access-Control-Allow-Origin: *\r\n" \
    "Cache-Control: no-store, no-cache, must-revalidate, pre-check=0, post-check=0, max-age=0\r\n" \
    "Pragma: no-cache\r\n" \
    "Expires: Mon, 3 Jan 2000 12:34:56 GMT\r\n"
```


I am greatly indebted to the very intelligent and talented Patrick Catanzariti for a lot of the code and ideas. http://www.sitepoint.com/streaming-a-raspberry-pi-camera-into-vr-with-javascript/.

Please also view the videos I have done on youtube https://www.youtube.com/watch?v=7L83WuqlDFc, https://www.youtube.com/watch?v=JmBJmMUzxHQ and https://www.youtube.com/watch?v=gterpj5ttcg

Initially I have used this with the wii mario cart as the driver of the robot but there are many improvements possible. Like using the motion api. I am also interested in developing augmented reality AR, so you could see a mixture of the real camera images and also 3d objects or creatures. We shall see.